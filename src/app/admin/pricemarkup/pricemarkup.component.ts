import { Component, OnInit } from '@angular/core';
import { FormArray, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-pricemarkup',
  templateUrl: './pricemarkup.component.html',
  styleUrls: ['./pricemarkup.component.css']
})
export class PricemarkupComponent implements OnInit {

  public stockUpdateForm: FormGroup;
  totalSum = 0;

  constructor(private _fb: FormBuilder,
    private currencyPipe: CurrencyPipe) { }

  /**
   * Form initialization
   */
  ngOnInit() {
    this.stockUpdateForm = this._fb.group({
      units: this._fb.array([
        this.getUnit()
      ])
    });
    // initialize stream
    const myFormValueChanges$ = this.stockUpdateForm.controls['units'].valueChanges;
    // subscribe to the stream
    myFormValueChanges$.subscribe(units => this.updateTotalUnitPrice(units));
  }

  /**
   * Create form unit
   */

  private getUnit() {
    const numberPatern = '^[0-9.,]+$';
    return this._fb.group({
      unitName: ['', Validators.required],
      qty: [1, [Validators.required, Validators.pattern(numberPatern)]],
      unitPrice: ['', [Validators.required, Validators.pattern(numberPatern)]],
      unitTotalPrice: [{ value: '', disabled: true }]
    });
  }

  /**
   * Add new unit row into form
   */

  private addUnit() {
    const control = <FormArray>this.stockUpdateForm.controls['units'];
    control.push(this.getUnit());
  }

  /**
   * Remove unit row from form on click delete button
   */


  private removeUnit(i: number) {
    const control = <FormArray>this.stockUpdateForm.controls['units'];
    control.removeAt(i);
  }

  private updateTotalUnitPrice(units: any) {
    // get our units group controll
    const control = <FormArray>this.stockUpdateForm.controls['units'];
    // before recount total price need to be reset.
    this.totalSum = 0;
    for (let i in units) {
      let totalUnitPrice = (units[i].qty * units[i].unitPrice);
      // now format total price with angular currency pipe
      let totalUnitPriceFormatted = this.currencyPipe.transform(totalUnitPrice, 'USD', 'symbol-narrow', '1.2-2');
      // update total sum field on unit
      control.at(+i).get('unitTotalPrice').setValue(totalUnitPriceFormatted, { onlySelf: true, emitEvent: false });
      // update total price for all units
      this.totalSum += totalUnitPrice;
    }
  }
}
