import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ProductUploadComponent } from './product-upload/product-upload.component';
import { PricemarkupComponent } from './pricemarkup/pricemarkup.component';

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  children: [{
    path: 'products',
    component: ProductUploadComponent
  },
  {
    path: 'productmarkup',
    component: PricemarkupComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
