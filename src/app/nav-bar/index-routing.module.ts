import { CommonModule } from '@angular/common';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavBarComponent } from './nav-bar.component';

const routes: Routes = [{
  path: '',
  component: NavBarComponent,
  children: [
    { path: 'products', loadChildren: './../products/products.module#ProductsModule' },
    { path: 'user', loadChildren: './../user/user.module#UserModule' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IndexRoutingModule { }
