
import { Routes } from '@angular/router';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { UserComponent } from './user/user.component';
export const appRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        loadChildren: './nav-bar/index.module#IndexModule'
      },
      {
        path: 'admin',
        loadChildren: './admin/admin.module#AdminModule'
      },
    ]
  },
  // { path: 'no-access', component: NoAccessComponent },
  // { path: '**', component: PageNotFoundComponent }
];

export class AppRoutingModule { }
